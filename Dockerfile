# Use a smaller base image
FROM node:20-alpine3.20

ARG SOURCE_REPOSITORY

# Install build dependencies
RUN apk add --no-cache git

# Using Code repository from Env Variable to switch between origianl and Verde's version
RUN git clone --depth 1 $SOURCE_REPOSITORY app/

# Set the working directory
WORKDIR /app

# Install only production dependencies to reduce image size
RUN npm ci --omit=dev

# Expose the port your app runs on
EXPOSE 3000

# Start the application
CMD ["npm", "start"]
