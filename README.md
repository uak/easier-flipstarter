# Easier Flipstarter 

Setup Flipstarter campaigns the easy way!

## Description
Want to create a Flipstarter campaign with SSL support with less hassle? This project is for you! It's based on [https-portal](https://github.com/SteveLTN/https-portal) and allows you to host the original [Flipstarter](https://gitlab.com/flipstarter/backend) or [SoftwareVerde](https://github.com/SoftwareVerde/flipstarter) Flipstarter.

⚠️ No warranty - Open Source - Standing on the shoulders of other projects

## Requirements

* Docker
* git

Check the [Docker Guide](#getting-docker) if you don't have it.

## Preparation

Clone the repository:

```bash
git clone https://gitlab.com/uak/easier-flipstarter
```

Change into the directory:

```bash
cd easier-flipstarter
```

Copy `env.sample` to `.env` file. Docker will read settings from  `.env`.

```bash
cp env.sample .env
```

### Setup Your Domain

* Change the domain in `.env` file to your own domain.

### Select Flipstarter Version

In the `.env` file you will see the two Flipstarter versions linked in the [Description](#description). To use the preferred one just uncomment it and comment the other option to disable it:

```
## Verde's Flipstarter
## This is commented out by default
# SOURCE_REPOSITORY=https://github.com/SoftwareVerde/flipstarter.git

## Original Flipstarter
## This is active by default
SOURCE_REPOSITORY=https://gitlab.com/flipstarter/backend.git
```


## First Run (Testing)

Inside the `easier-flipstarter` run

```
docker compose up
```

The app should run using staging (temporary test) certificate because `STAGE` is set to `staging` in `.env`

If you visit the domain you will see it shows a certificate warning which is ok for now.

### Verifying 

The campaign will handle real money, and it will look unprofessional if you publicize something that is not yet working. Therefore it's strongly suggested to test deployment and funding before you publish a campaign. Few tests you should run are:

1. Test contributing to the campaign.
2. Check that your campaign persists after shutting down and restarting the campaign.
    * Run `docker compose down` to turn off the campaign.
    * Run `docker compose up` to start it again.
    * Check that the previous pledge is still there and everything looks fine.
3. Test fully funding with a test campaign with small amount of BCH.
4. When you are done with testing, clear the campaign data by running `docker compose down -v`

## Production (Real campaign)

Change the `STAGE` from **staging** to **production** in `.env` file then rerun the campaign using 

```
docker compose up -d
```

The `-d` flag will make sure it runs in the background. You should now have your campaign running with proper SSL certificate issued by Let's Encrypt. 


## Tests

Tested with:
* Node.js v20.9.0

## Notes

If you want to setup the server to be more secure you can follow this guide: [Initial Server Setup with Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-22-04) which is a recommended guide for setting up a server before running production services.

### Getting Docker

If you need Docker

* Rent a server from [ServersGuru](https://my.servers.guru/) and rebuild it using Docker-ce image.
* Create [Docker Droplet from Digital Ocean](https://marketplace.digitalocean.com/apps/docker) market place
* [install it yourself](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04) on Ubuntu server using Digital Ocean guide.

### Resetting The Campaign

If you want to remove the campaign and start from zero you run:

```
docker compose down -v
```

⚠️ This will delete everything including all the pledges and start with new empty campaign.

### SSL Certificates

The [https-portal](https://github.com/SteveLTN/https-portal) application takes care of obtaining certificates automatically. Usage of 
Let's Encrypt certificates has limits on real certificate requests so we start by requesting a staging certificate to make sure our domain is reachable and able to get a temporally certificate before requesting a real certificate.

To

## Support
For support you can ask in the general Flipstarter community [Flipstarter](https://t.me/flipstarter) or [BCH Devs & Builders](https://t.me/bchbuilders) Telegram group.

## Contributing
Merge requests are welcomed

## Authors and acknowledgment
By @uak

## License
Public Domain
